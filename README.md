# Backend Challenges boilerplate - package.json

[![Run on Repl.it](https://repl.it/badge/github/freeCodeCamp/boilerplate-npm)](https://repl.it/github/freeCodeCamp/boilerplate-npm)

## Notas

* `npm` - Es el gestor de paquetes para Node.js
* `package.json` - Es un archivo JSON que contiene información de un proyecto o paquete npm de JavaScript o Node.js
  * Los campos `name` y `version` son obligatorios.
  * `author` - Es un string o array de objetos que describe quién o quienes desarrollaron el proyecto.
  * `description` - Contiene una descripción corta e informativa del proyecto.
  * `keywords` - Es un array de strings de tags que describen al proyecto.
  * `license` - Es un string que contiene la licencia de software del proyecto.
  * `version` - Es un string que contiene la versión actual del proyecto.
  * `dependencies` - Es un objeto que contiene todos los paquetes que necesita el proyecto `"package": "version"`
    * La versión de los paquetes de dependencias siguen el llamado Versionado Semántico (SemVer) `"package": "MAJOR.MINOR.PATCH"`
    * `MAJOR` - Representan cambios de API incompatibles (versión mayor).
    * `MINOR` - Representan nuevas funcionalidades compatibles con versiones anteriores (versión menor).
    * `PATCH` - Representan corrección de errores compatibles con versiones anteriores (parches).
    * `"package": "~1.3.8"` o `"package": "1.3.x"` - Le permite a npm actualizar correcciones de errores a una dependencia.
    * `"package": "^1.3.8"` o `"package": "1.x.x"` - Le permite a npm actualizar correcciones de errores y agregar nuevas funcionalidades a una dependencia.
* Para eliminar dependencias de un proyecto, se elimina el par clave-valor correspondiente al paquete de dependencias del archivo `package.json`
  * Este método es aplicable para eliminar otros campos del archivo `package.json`

## Referencias

* [Node.js](https://nodejs.org/es/)
* [npm](https://www.npmjs.com/)
* [package.json](https://docs.npmjs.com/cli/v8/configuring-npm/package-json)
